import React from 'react';
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import PostMessage from "./containers/PostMessage/PostMessage";
import GetMessages from "./containers/GetMessges/GetMessages";

const App = () => (
  <>
    <CssBaseline/>

    <main>
      <Container maxWidth="xl">
            <PostMessage/>
            <GetMessages/>
      </Container>
    </main>
  </>
);

export default App;
